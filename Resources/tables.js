var c1 = require('mouse');
var c2 = require('slider');


var view = Ti.UI.createView();

var data = [

	{title: 'No place for Mickey Mouse!', hasChild: true, height: 100, backgroundColor: 'white', color: 'black'},
	{title: "Gliding n' sliding", hasChild: true, height: 100, backgroundColor: 'white', color: 'black'},
];

var table = Ti.UI.createTableView({
	data: data,
	width: Ti.UI.MAX,
});

//window.add(table);
view.add(table);

table.addEventListener('click',function(e){
	if(e.index === 0)
	{
		//alert(e.index);
		c1.win.open();
	}
	else if(e.index === 1)
	{
		//alert(e.index);
		c2.win.open();
	}
});

view.add(table);

//exports this hole stuff on a view
exports.view = view;
