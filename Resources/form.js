var data = {
	'Diego': 28,
	'Renan': 30,
	'Vinicis': 25,
	'Natalia': 27,
	'Cecilia': 20
};

var view = Ti.UI.createView();

var file = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory,'data.txt');

if(!file.exists)
{
	file.createFile();
}

//file.write(JSON.stringfy(data));

//var content = file.read();

//Ti.API.info(JSON.parse(content));

var textfield = Ti.UI.createTextField({
	color: '#333333',
	hintText: 'First name',
	hintTextColor: '#000000',
	height: Ti.UI.MAX,
	top: 40,
	left: 10,
	width: 200,
	backgroundColor: '#ffffff',
	borderStyle: Ti.UI.INPUT_BORDERSTYLE_BEZEL
});

view.add(textfield);

//creates a button
var btn = Ti.UI.createButton({
	title: 'Submit',
	top: 38,
	width: Ti.UI.MAX,
	left:'65%',
	height: Ti.UI.MAX
});

//creates an event for it
btn.addEventListener('click', function(e){
	alert(textfield.getValue());
	textfield.setValue('');
});

//adds it to the view
view.add(btn);



exports.view = view;
