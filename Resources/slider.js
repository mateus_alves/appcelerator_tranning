var win = Ti.UI.createWindow({
		backgroundColor: '#0f0f0'
});


//creating first slider, label and event!
var slider = Titanium.UI.createSlider({
    top: 50,
    min: 0,
    max: 100,
    width: '100%',
    value: 50,
    });

var label = Ti.UI.createLabel({
    text: slider.value,
    width: '100%',
    top: 30,
    left: 0,
    textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
    });

slider.addEventListener('change', function(e) {
    label.text = String.format("%3.1f", e.value);
});

//adding second slider, label and event

//win.add(label);
//win.add(slider);

exports.win = win;