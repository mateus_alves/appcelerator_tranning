var Map = require('ti.map');
var view = Ti.UI.createView();

var ann = Map.createAnnotation({
    latitude:-22.899220,
    longitude:-43.178900,
    title:"Lugar 0",
    subtitle:'Rio de Janeiro',
    pincolor:Map.ANNOTATION_RED,
    myid:1 // Custom property to uniquely identify this annotation.
});

var ann2 = Map.createAnnotation({
    latitude:-22.898220,
    longitude:-43.178900,
    title:"Lugar 1",
    subtitle:'Rio de Janeiro',
    pincolor:Map.ANNOTATION_BLUE,
    myid:1 // Custom property to uniquely identify this annotation.
});

var ann3 = Map.createAnnotation({
    latitude:-22.894220,
    longitude:-43.178900,
    title:"Lugar 3",
    subtitle:'Rio de Janeiro',
    pincolor: Map.ANNOTATION_GREEN,
    myid:1 // Custom property to uniquely identify this annotation.
});


var mapview = Map.createView({
	top: Ti.UI.MAX,
	height: Ti.UI.MAX,
	//mapType: Ti.Map.STANDARD_TYPE, 
	mapType: Map.NORMAL_TYPE,
	region: {
		latitude: -22.899877,
		longitude: -43.178935,
		latitudeDelta: 0.002,
		longitudeDelta: 0.002,
	}, 
	animate:true,
	regionFit: true,
	userLocation: true,
	annotations:[ann,ann2,ann3]
});

view.add(mapview);




//exports to app.js
exports.view = view;

